FROM python:3.7-slim
COPY . /app
WORKDIR /app
RUN apt-get update -qy && \
    apt-get install -qq -y \
    build-essential libpq-dev && \
    apt-get clean && \
    pip install --upgrade pip && \
    pip install -r ./requirements.txt
EXPOSE 5001
CMD python ./run.py
