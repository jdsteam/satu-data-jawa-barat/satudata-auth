
import logging.config
import routes
from flask import Flask
from flask_mail import Mail
from flask_cors import CORS
from flask_compress import Compress
from flask_socketio import SocketIO
from flask_swagger_ui import get_swaggerui_blueprint

from helpers import jwtmanager
from helpers import postgre_alchemy

cors = CORS()
compress = Compress()
mail = Mail()
socketio = SocketIO(async_mode='threading', ping_timeout=7200)

def create_app(configuration):

    app = Flask(
        __name__.split(',')[0],
        static_url_path='/static',
        static_folder='../static',
        template_folder='../template')

    # swagger specific server
    SWAGGER_URL = '/api-auth'
    API_URL = '/api-auth/static/swagger.json'
    SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(
        SWAGGER_URL,
        API_URL,
        config={
            'app_name': "Satu Data Auth",
            'base_url': ''
        }
    )

    # swagger specific local
    # SWAGGER_URL = ''
    # API_URL = '/static/swagger.json'
    # SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(
    #     SWAGGER_URL,
    #     API_URL,
    #     config={
    #         'app_name': "Satu Data Auth",
    #         'base_url': ''
    #     }
    # )

    # register route blueprint
    app.register_blueprint(SWAGGERUI_BLUEPRINT, url_prefix="/")
    app.register_blueprint(routes.index.bp)
    app.register_blueprint(routes.error.bp)
    app.register_blueprint(routes.auth.bp)
    app.register_blueprint(routes.role.bp)
    app.register_blueprint(routes.user.bp)
    app.register_blueprint(routes.app.bp)
    app.register_blueprint(routes.app_service.bp)
    app.register_blueprint(routes.user_permission.bp)
    app.register_blueprint(routes.user_app_role.bp)
    app.register_blueprint(routes.agreement.bp)

    # load configuration
    app.config.from_object(configuration)

    # init app
    cors.init_app(app, resources={r"/*": {"origins": "*"}})
    compress.init_app(app)
    socketio.init_app(app)
    jwtmanager.init_app(app)
    mail.init_app(app)

    postgre_alchemy.init_app(app)

    return app
