import requests
import jwt
import json
import time
from flask import current_app, request
from functools import wraps
from helpers import Helper
from exceptions import BadRequest
from exceptions import ErrorMessage
from exceptions import SuccessMessage
from flask import Response
from helpers.jsonencoder import JSONEncoder
from settings import configuration

helper = Helper()


def jwt_check():

    def wrapper(func):

        @wraps(func)
        def decorator(*args, **kwargs):

            token = request.headers.get('Authorization', None)
            app_code = configuration.app_code
            controller = request.path
            action = request.method

            if(controller[0] == '/'):
                controller = controller[(len(controller)-1)*-1:]

            res = ''
            fullurl = configuration.rolecheck_url + 'auth/check?app_code=' + str(app_code) + '&controller=' + str(controller) + '&action=' + str(action)
            
            while res == '':
                try:
                    req = requests.get(fullurl, headers={"Authorization":token})
                    break
                except:
                    print("Connection refused by the server..")
                    print("Let me sleep for 5 seconds")
                    print("ZZzzzz...")
                    time.sleep(5)
                    print("Was a nice sleep, now let me continue...")
                    continue
                
            res = req.json()
          
            if(res['error'] > 0):
                return Response(json.dumps(res, cls=JSONEncoder), mimetype='application/json'), 403
            else:
                pass

            return func(*args, **kwargs)

        return decorator

    return wrapper
