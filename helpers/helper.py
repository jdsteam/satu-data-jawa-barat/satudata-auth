import time, os
import datetime, pytz
import jwt
import json
from passlib.hash import pbkdf2_sha256
from settings import configuration
from flask import Response
from flask import request
from helpers.jsonencoder import JSONEncoder
from flask_mail import Message, Mail

class Helper(object):

    def date_to_timestamp(self, date: str):

        timestamp = time.mktime(datetime.datetime.strptime(
            date, "%Y-%m-%dT%H:%M:%S%z").timetuple())

        return int(timestamp)

    @staticmethod
    def generate_hash(password):
        return pbkdf2_sha256.hash(password)

    @staticmethod
    def verify_hash(password, hash):
        return pbkdf2_sha256.verify(password, hash)

    @staticmethod
    def jwt_encode(payload: dict):
        payload["iat"] = datetime.datetime.utcnow()
        payload["exp"] = datetime.datetime.utcnow() + \
            datetime.timedelta(days=1)
        return jwt.encode(payload, configuration.jwt_secret_key, algorithm=configuration.jwt_algorithm).decode("utf-8")

    @staticmethod
    def read_jwt():
        try:
            token = request.headers.get('Authorization', None)
            tokens = token.split(" ")
            payload = jwt.decode(tokens[1], configuration.jwt_secret_key, algorithm=[configuration.jwt_algorithm])
            return payload
        except Exception as e:
            return {}
        
    
    @staticmethod
    def localDate():
        try:
            ServerTime = (datetime.datetime.utcnow() + datetime.timedelta(minutes=18, seconds=34)).replace(tzinfo=pytz.utc).astimezone(pytz.timezone("Asia/Jakarta")).strftime('%Y-%m-%d %H:%M:%S')
            return ServerTime
            
        except Exception as e:
            print(e)

    @staticmethod
    def sendEmail(subject,recipients=dict,html=None):
        mail = Mail()
        msg = Message()
        try: 
            
            msg.subject=subject
            msg.sender=os.getenv("MAIL_SENDER")
            msg.html=html
            msg.recipients=recipients
            return mail.send(msg)

        except Exception as e:
            # fail response
            print(e)