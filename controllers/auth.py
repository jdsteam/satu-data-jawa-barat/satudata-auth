import json, pytz, string, random
from helpers import Helper
from datetime import datetime
from dateutil import parser
from exceptions import BadRequest, ErrorMessage, SuccessMessage
from helpers.jsonencoder import JSONEncoder
from models import AppModel, AppServiceModel, RegionalModel
from models import RegionalLevelModel, RoleModel, SkpdModel
from models import SkpdSubModel, SkpdUnitModel, UserModel
from models import UserAppRoleModel, UserPermissionModel, DatasetClassModel
from models import AgreementModel
from helpers.postgre_alchemy import postgre_alchemy as db
from controllers import UserController

userCtrl = UserController()
helper = Helper()

class AuthController(object):

    def __init__(self, **kwargs):
        pass

    def login(self, json_data: dict):
        try:
            # query postgresql
            json_send = {}
            if("username" not in json_data):
                raise ErrorMessage("Username is missing", 500, 1, {})

            if("password" not in json_data):
                raise ErrorMessage("Password is missing", 500, 1, {})

            if("app_code" not in json_data):
                raise ErrorMessage("App Code is missing", 500, 1, {})

            # query
            result = db.session.query(UserAppRoleModel.id.label('user_app_role_id'),
                UserModel.id, UserModel.email, UserModel.username, UserModel.password, UserModel.nip,
                UserModel.kode_kemendagri, UserModel.kode_skpd, UserModel.kode_skpdsub, UserModel.kode_skpdunit,
                UserModel.name, UserModel.profile_pic, UserModel.notes, UserModel.last_login,
                UserModel.cdate, UserModel.cuid, UserModel.is_active, UserModel.is_deleted,
                RoleModel.id.label('role_id'), RoleModel.name.label('role_name'),
                AppModel.id.label('app_id'), AppModel.name.label('app_name'), AppModel.url.label('app_url'), AppModel.code.label('app_code'),
                RegionalLevelModel.name.label('regional_level'), RegionalModel.nama_kemendagri,
                SkpdModel.nama_skpd, SkpdSubModel.nama_skpdsub, SkpdUnitModel.nama_skpdunit, SkpdModel.is_external, UserModel.profile_pic, SkpdModel.logo,
                UserModel.agreement_id)
            result = result.outerjoin(UserModel, UserAppRoleModel.user_id == UserModel.id)
            result = result.outerjoin(AppModel, UserAppRoleModel.app_id == AppModel.id)
            result = result.outerjoin(RoleModel, UserAppRoleModel.role_id == RoleModel.id)
            result = result.outerjoin(RegionalModel, UserModel.kode_kemendagri == RegionalModel.kode_kemendagri)
            result = result.outerjoin(RegionalLevelModel, RegionalModel.regional_level_id == RegionalLevelModel.id)
            result = result.outerjoin(SkpdModel, UserModel.kode_skpd == SkpdModel.kode_skpd)
            result = result.outerjoin(SkpdSubModel, UserModel.kode_skpdsub == SkpdSubModel.kode_skpdsub)
            result = result.outerjoin(SkpdUnitModel, UserModel.kode_skpdunit == SkpdUnitModel.kode_skpdunit)
            result = result.filter(UserModel.username == json_data['username'])
            result = result.filter(AppModel.code == json_data['app_code'])
            result = result.filter(UserModel.is_deleted == False)
            result = result.all()


            user = []
            for res in result:
                temp = {}
                temp['user_app_role_id'] = res[0]
                temp['id'] = res[1]
                temp['email'] = res[2]
                temp['username'] = res[3]
                temp['password'] = res[4]
                temp['nip'] = res[5]
                temp['kode_kemendagri'] = res[6]
                temp['kode_skpd'] = res[7]
                temp['kode_skpdsub'] = res[8]
                temp['kode_skpdunit'] = res[9]
                temp['name'] = res[10]
                temp['profile_pic'] = res[11]
                temp['notes'] = res[12]
                temp['last_login'] = res[13]
                temp['cdate'] = res[14]
                temp['cuid'] = res[15]
                temp['is_active'] = res[16]
                temp['is_deleted'] = res[17]
                temp['role_id'] = res[18]
                temp['role_name'] = res[19]
                temp['app_id'] = res[20]
                temp['app_name'] = res[21]
                temp['app_url'] = res[22]
                temp['app_code'] = res[23]
                temp['regional_level'] = res[24]
                temp['nama_kemendagri'] = res[25]
                temp['nama_skpd'] = res[26]
                temp['nama_skpdsub'] = res[27]
                temp['nama_skpdunit'] = res[28]
                temp['is_external'] = res[29]
                temp['profile_pic'] = res[30]
                temp['logo_skpd'] = res[31]
                temp['agreement_id'] = res[32]
                user.append(temp)

            # check if empty
            if user:
                user = user[0]
                user_pass_ok = helper.verify_hash(
                    json_data["password"], user["password"])
                if(user_pass_ok):

                    forjwt = {}
                    forjwt["id"] = str(user["id"])
                    forjwt["username"] = user["username"]
                    forjwt["email"] = user["email"]
                    forjwt["name"] = user["name"]
                    forjwt["regional_level"] = user["regional_level"]
                    forjwt["kode_kemendagri"] = user["kode_kemendagri"]
                    forjwt["nama_kemendagri"] = user["nama_kemendagri"]

                    forjwt["profile_pic"] = user["profile_pic"]
                    if(forjwt["profile_pic"] == None):
                        forjwt["profile_pic"] = ""
                    else:
                        forjwt["profile_pic"] = user["profile_pic"]

                    forjwt["logo_skpd"] = user["logo_skpd"]
                    if(forjwt["logo_skpd"] == None):
                        forjwt["logo_skpd"] = ""
                    else:
                        forjwt["logo_skpd"] = user["logo_skpd"]

                    forjwt["kode_skpd"] = user["kode_skpd"]
                    if(forjwt["kode_skpd"] == ""):
                        forjwt["nama_skpd"] = ""
                    else:
                        forjwt["nama_skpd"] = user["nama_skpd"]

                    forjwt["kode_skpdsub"] = user["kode_skpdsub"]
                    if(forjwt["kode_skpdsub"] == ""):
                        forjwt["nama_skpdsub"] = ""
                    else:
                        forjwt["nama_skpdsub"] = user["nama_skpdsub"]

                    forjwt["kode_skpdunit"] = user["kode_skpdunit"]
                    if(forjwt["kode_skpdunit"] == ""):
                        forjwt["nama_skpdunit"] = ""
                    else:
                        forjwt["nama_skpdunit"] = user["nama_skpdunit"]
                    forjwt["is_external"] = user["is_external"]

                    forjwt2 = {}
                    forjwt2["id"] = str(user["role_id"])
                    forjwt2["name"] = str(user["role_name"])
                    forjwt3 = {}
                    forjwt3["id"] = str(user["app_id"])
                    forjwt3["name"] = str(user["app_name"])
                    forjwt3["code"] = str(user["app_code"])
                    forjwt3["url"] = str(user["app_url"])
                    forjwt["app"] = forjwt3
                    forjwt["role"] = forjwt2
                    jwt = helper.jwt_encode(forjwt)

                    result = {}
                    result["id"] = str(user["id"])
                    result["username"] = user["username"]
                    result["email"] = user["email"]
                    result["name"] = user["name"]
                    result["regional_level"] = user["regional_level"]
                    result["kode_kemendagri"] = user["kode_kemendagri"]
                    result["nama_kemendagri"] = user["nama_kemendagri"]

                    result["profile_pic"] = user["profile_pic"]
                    if(result["profile_pic"] == None):
                        result["profile_pic"] = ""
                    else:
                        result["profile_pic"] = user["profile_pic"]

                    result["logo_skpd"] = user["logo_skpd"]
                    if(result["logo_skpd"] == None):
                        result["logo_skpd"] = ""
                    else:
                        result["logo_skpd"] = user["logo_skpd"]

                    result["kode_skpd"] = user["kode_skpd"]
                    if(result["kode_skpd"] == ""):
                        result["nama_skpd"] = ""
                    else:
                        result["nama_skpd"] = user["nama_skpd"]

                    result["kode_skpdsub"] = user["kode_skpdsub"]
                    if(result["kode_skpdsub"] == ""):
                        result["nama_skpdsub"] = ""
                    else:
                        result["nama_skpdsub"] = user["nama_skpdsub"]

                    result["kode_skpdunit"] = user["kode_skpdunit"]
                    if(result["kode_skpdunit"] == ""):
                        result["nama_skpdunit"] = ""
                    else:
                        result["nama_skpdunit"] = user["nama_skpdunit"]

                    result["cdate"] = user["cdate"]
                    result["last_login"] = user["last_login"]
                    result["is_deleted"] = user["is_deleted"]
                    result["is_external"] = user["is_external"]

                    result2 = {}
                    result2["id"] = str(user["role_id"])
                    result2["name"] = str(user["role_name"])
                    result3 = {}
                    result3["id"] = str(user["app_id"])
                    result3["name"] = str(user["app_name"])
                    result3["code"] = str(user["app_code"])
                    result3["url"] = str(user["app_url"])
                    result["app"] = result3
                    result["role"] = result2
                    result["jwt"] = str(jwt)
                    result["agreement_id"] = user['agreement_id']

                    if result['agreement_id']:
                        result_agreement = db.session.query(AgreementModel)
                        result_agreement = result_agreement.get(result['agreement_id'])
                        if result_agreement:
                            result_agreement = result_agreement.__dict__
                            if result_agreement['is_active'] == True:
                                result['is_agree'] = True
                            else:
                                result['is_agree'] = False
                        else:
                            result['is_agree'] = False
                    else:
                        result['is_agree'] = False

                    return True, result, ""

                    # else:
                    #     return False, {}, "Role not found"
                else:
                    return False, {}, "Password not match"
            else:
                return False, {}, "Username or App Code not found"

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def check_is_backend(self, app_code:str, controller:str, action:str):
        try:
            # query postgresql
            result = db.session.query(AppModel.id, AppModel.name, AppModel.code, AppModel.url,
                AppServiceModel.controller, AppServiceModel.action, AppServiceModel.is_backend, AppServiceModel.dataset_class_id)
            result = result.join(AppServiceModel, AppModel.id == AppServiceModel.app_id)
            result = result.filter(AppModel.code == str(app_code))
            result = result.filter(AppServiceModel.controller == str(controller))
            result = result.filter(AppServiceModel.action == str(action))
            # result = result.limit(1)
            result = result.all()


            results = []
            for res in result:
                temp = {}
                temp['id'] = res[0]
                temp['name'] = res[1]
                temp['code'] = res[2]
                temp['url'] = res[3]
                temp['controller'] = res[4]
                temp['action'] = res[5]
                temp['is_backend'] = res[6]
                temp['dataset_class_id'] = res[7]

                if(temp['dataset_class_id'] != None):
                    # get relation to skpd
                    dataset_class = db.session.query(DatasetClassModel)
                    dataset_class = dataset_class.get(temp['dataset_class_id'])

                    if(dataset_class):
                        dataset_class = dataset_class.__dict__
                        dataset_class.pop('_sa_instance_state', None)
                    else:
                        dataset_class = {}

                    temp['dataset_class'] = dataset_class

                else:
                    temp['dataset_class'] = {}

                results.append(temp)


            # check if empty
            if results:
                return True, results[0]
            else:
                return False, {}

        except Exception as e:
            # fail response
            print(e)
            raise ErrorMessage(str(e), 500, 1, {})

    def permission_by_userid(self, id:str, app_code:str, controller:str, action:str):
        try:
            # query postgresql
            result = db.session.query(UserPermissionModel.id, UserPermissionModel.user_id, UserPermissionModel.app_id,
                AppModel.name, AppModel.url, AppModel.code.label('app_code'),
                UserPermissionModel.app_service_id, AppServiceModel.controller, AppServiceModel.action,
                UserPermissionModel.enable, UserPermissionModel.notes, UserPermissionModel.cdate, UserPermissionModel.cuid)
            result = result.join(AppModel, UserPermissionModel.app_id == AppModel.id)
            result = result.join(AppServiceModel, UserPermissionModel.app_service_id == AppServiceModel.id)
            result = result.filter(UserPermissionModel.user_id == str(id))
            result = result.filter(AppModel.code == str(app_code))
            result = result.filter(AppServiceModel.controller == str(controller))
            result = result.filter(AppServiceModel.action == str(action))
            # result = result.limit(1)
            # print(result)
            result = result.all()

            permission = []
            for res in result:
                temp = {}
                temp['id'] = res[0]
                temp['user_id'] = res[1]
                temp['app_id'] = res[2]
                temp['name'] = res[3]
                temp['url'] = res[4]
                temp['app_code'] = res[5]
                temp['app_service_id'] = res[6]
                temp['controller'] = res[7]
                temp['action'] = res[8]
                temp['enable'] = res[9]
                temp['notes'] = res[10]
                temp['cdate'] = res[11]
                temp['cuid'] = res[12]
                permission.append(temp)
            # print(permission)

            # check if empty
            if permission:
                return True, permission
            else:
                return False, {}

        except Exception as e:
            # fail response
            print(e)
            raise ErrorMessage(str(e), 500, 1, {})

    def resetPassword(self,json_request):
        try:
            result = db.session.query(UserModel)
            result = result.filter(UserModel.username == json_request['username'])
            result = result.filter(UserModel.email == json_request['email'])

            user = []
            for res in result:
                res = res.__dict__
                res.pop('_sa_instance_state', None)
                user.append(res['id'])

            if user:
                randomStr = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))
                userCtrl.update({'id':user[0]},{'password':randomStr})

                email = [json_request['email']]
                html = '<body style="background-color: #e9ecef;"><br><br><br><br>'
                html +='<table border="0" cellpadding="0" cellspacing="0" width="100%">'
                html +='<tr><td align="center" bgcolor="#e9ecef"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;"><tr>'
                html +='<td align="left" bgcolor="#ffffff" style="padding: 36px 24px 0; border-top: 3px solid #d4dadf;">'
                html +='<h1 style="margin: 0; font-size: 32px; font-weight: 700; letter-spacing: -1px; line-height: 48px;">Reset Your Password</h1></td></tr></table></td></tr>'
                html +='<tr><td align="center" bgcolor="#e9ecef"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">'
                html += '<tr><td align="left" bgcolor="#ffffff" style="padding: 24px; font-size: 16px; line-height: 24px;">'
                html += '<p style="margin: 0;margin-bottom: -20px;">Berikut adalah password sementara yang telah digenerate oleh system. '
                html +='<strong>'+randomStr+'</strong></p></td></tr>'
                html +='<tr><td align="left" bgcolor="#ffffff" style="padding: 24px; font-size: 16px; line-height: 24px;"><p style="margin: 0;"> Segera lakukan perubahan password setelah melakukan Login, <br> dengan cara : <br>1. Masuk ke User Profile<br>2. Klik tombol Ubah Password<br>3. Isi Form berisi Password Lama, Password Baru dan Ulangi Password Baru<br>4. Lakukan Submit</p></td></tr>'
                html +='</table></td></tr></table></td></tr><tr><td align="center" bgcolor="#e9ecef" style="padding: 24px;"></td></tr></table></body></html>'
                send = helper.sendEmail("User Reset Password",email,html)

                return True, send
            else:
                return False, "Username and Email Belum Sesuai."

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})
