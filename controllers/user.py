from settings.configuration import Configuration
from helpers import Helper
from datetime import datetime
from dateutil import parser
from exceptions import BadRequest
from exceptions import ErrorMessage
from exceptions import SuccessMessage
from models import UserModel
from models import RoleModel
from models import AppModel
from models import UserAppRoleModel
from models import SkpdModel
from models import SkpdSubModel
from models import SkpdUnitModel
from models import AgreementModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy.orm import defer
from sqlalchemy import or_
from sqlalchemy import join
from sqlalchemy import func
from sqlalchemy import cast
import sqlalchemy, pytz


configuration = Configuration()
helper = Helper()

# referensi query model sql alchemy
# https://docs.sqlalchemy.org/en/latest/orm/query.html


class UserController(object):

    def __init__(self, **kwargs):
        pass

    def get_all(self, where: dict, search, sort, limit, skip):

        try:
            # query postgresql
            result = db.session.query(UserModel)
            result = result.outerjoin(SkpdModel, UserModel.kode_skpd == SkpdModel.kode_skpd)
            result = result.outerjoin(UserAppRoleModel, UserModel.id == UserAppRoleModel.user_id)
            result = result.outerjoin(RoleModel, UserAppRoleModel.role_id == RoleModel.id)
            for attr, value in where.items():
                result = result.filter(getattr(UserModel, attr) == value)
            result = result.filter(or_(
                cast(getattr(UserModel, "username"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(UserModel, "email"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(UserModel, "name"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(RoleModel, "title"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(RoleModel, "name"), sqlalchemy.String).ilike('%'+search+'%'),
                ))
            # result = result.filter(or_(cast(getattr(UserModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in UserModel.__table__.columns ))
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.options(
                defer("password"))
            # result = result.all()
            result = result.distinct()

            # change into dict
            user = []
            for res in result:
                res = res.__dict__
                res.pop('_sa_instance_state', None)

                # get relation to skpd
                if(res['kode_skpd'] != None):
                    if(len(res['kode_skpd'].split()) > 0):
                        skpd = db.session.query(SkpdModel)
                        skpd = skpd.options(
                            defer("cuid"),
                            defer("cdate"),
                            defer("muid"),
                            defer("mdate"),
                            defer("notes"))
                        skpd = skpd.filter(getattr(SkpdModel, "kode_skpd") == res['kode_skpd'])
                        skpd = skpd.one()

                        if(skpd):
                            skpd = skpd.__dict__
                            skpd.pop('_sa_instance_state', None)
                        else:
                            skpd = {}

                        res['skpd'] = skpd
                    else:
                        res['skpd'] = {}
                else:
                    res['skpd'] = {}

                # get relation to skpdsub
                if(res['kode_skpdsub'] != None):
                    if(len(res['kode_skpdsub'].split()) > 0):
                        skpdsub = db.session.query(SkpdSubModel)
                        skpdsub = skpdsub.options(
                            defer("cuid"),
                            defer("cdate"))
                        skpdsub = skpdsub.filter(getattr(SkpdSubModel, "kode_skpd") == res['skpd']['kode_skpd'])
                        skpdsub = skpdsub.filter(getattr(SkpdSubModel, "kode_skpdsub") == res['kode_skpdsub'])
                        skpdsub = skpdsub.limit(1)
                        skpdsub = skpdsub.one()

                        if(skpdsub):
                            skpdsub = skpdsub.__dict__
                            skpdsub.pop('_sa_instance_state', None)
                        else:
                            skpdsub = {}

                        res['skpdsub'] = skpdsub
                    else:
                        res['skpdsub'] = {}
                else:
                    res['skpdsub'] = {}

                # get relation to skpdunit
                if(res['kode_skpdunit'] != None):
                    if(len(res['kode_skpdunit'].split()) > 0):
                        skpdunit = db.session.query(SkpdUnitModel)
                        skpdunit = skpdunit.options(
                            defer("cuid"),
                            defer("cdate"))
                        skpdunit = skpdunit.filter(getattr(SkpdUnitModel, "kode_skpd") == res['skpd']['kode_skpd'])
                        skpdunit = skpdunit.filter(getattr(SkpdUnitModel, "kode_skpdsub") == res['skpdsub']['kode_skpdsub'])
                        skpdunit = skpdunit.filter(getattr(SkpdUnitModel, "kode_skpdunit") == res['kode_skpdunit'])
                        skpdunit = skpdunit.limit(1)
                        skpdunit = skpdunit.one()

                        if(skpdunit):
                            skpdunit = skpdunit.__dict__
                            skpdunit.pop('_sa_instance_state', None)
                        else:
                            skpdunit = {}

                        res['skpdunit'] = skpdunit
                    else:
                        res['skpdunit'] = {}
                else:
                    res['skpdunit'] = {}

                # get relation to role
                role = db.session.query(UserAppRoleModel)
                role = role.options(
                    defer("cuid"),
                    defer("cdate"),
                    defer("user_id"),
                    defer("notes"))
                role = role.filter(getattr(UserAppRoleModel, "user_id") == res['id'])
                role = role.all()

                roles = []
                apps = []
                for role in role:
                    if(role):
                        role = role.__dict__
                        role.pop('_sa_instance_state', None)
                        role.pop('id', None)

                        # get relation to rl
                        rl = db.session.query(RoleModel)
                        rl = rl.options(
                            defer("cuid"),
                            defer("cdate"))
                        rl = rl.get(role['role_id'])
                        role.pop('role_id', None)

                        if(rl):
                            rl = rl.__dict__
                            rl.pop('_sa_instance_state', None)
                            role['id'] = rl['id']
                            role['name'] = rl['name']
                            role['title'] = rl['title']
                        else:
                            role['id'] = ''
                            role['name'] = ''
                            role['title'] = ''

                        # get relation to app
                        app = db.session.query(AppModel)
                        app = app.options(
                            defer("cuid"),
                            defer("cdate"),
                            defer("is_deleted"),
                            defer("is_active"))
                        app = app.get(role['app_id'])
                        role.pop('app_id', None)

                        if(app):
                            app = app.__dict__
                            app.pop('_sa_instance_state', None)
                            app['name'] = app['name']
                            app['name'] = app['name']
                            app['code'] = app['code']
                            app['url'] = app['url']
                        else:
                            app['name'] = ''
                            app['code'] = ''
                            app['url'] = ''

                    else:
                        role = {}

                    roles.append(role)
                    apps.append(app)

                res['app'] = apps
                res['role'] = roles

                if res['agreement_id']:
                    result_agreement = db.session.query(AgreementModel)
                    result_agreement = result_agreement.get(res['agreement_id'])
                    if result_agreement:
                        result_agreement = result_agreement.__dict__
                        if result_agreement['is_active'] == True:
                            res['is_agree'] = True
                        else:
                            res['is_agree'] = False
                    else:
                        res['is_agree'] = False
                else:
                    res['is_agree'] = False

                user.append(res)

            # check if empty
            user = list(user)
            if (len(user) > 0):
                return True, user
            else:
                return False, []

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def get_count(self, where: dict, search):

        try:
            # query postgresql
            result = db.session.query(UserModel.id)
            result = result.outerjoin(SkpdModel, UserModel.kode_skpd == SkpdModel.kode_skpd)
            result = result.outerjoin(UserAppRoleModel, UserModel.id == UserAppRoleModel.user_id)
            result = result.outerjoin(RoleModel, UserAppRoleModel.role_id == RoleModel.id)
            for attr, value in where.items():
                result = result.filter(getattr(UserModel, attr) == value)
            result = result.filter(or_(
                cast(getattr(UserModel, "username"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(UserModel, "email"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(UserModel, "name"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(RoleModel, "title"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(RoleModel, "name"), sqlalchemy.String).ilike('%'+search+'%'),
                ))
            result = result.distinct()
            result = result.count()

            # change into dict
            user = {}
            user['count'] = result

            # check if empty
            if user:
                return True, user
            else:
                return False, {}

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def get_by_id(self, id):

        try:
            # execute database
            result = db.session.query(UserModel)
            result = result.options(
                defer("password"))
            result = result.get(id)

            if(result):
                result = result.__dict__
                result.pop('_sa_instance_state', None)

                # get relation to skpd
                if(result['kode_skpd'] != None):
                    if(len(result['kode_skpd'].split()) > 0):
                        skpd = db.session.query(SkpdModel)
                        skpd = skpd.options(
                            defer("cuid"),
                            defer("cdate"),
                            defer("muid"),
                            defer("mdate"),
                            defer("notes"))
                        skpd = skpd.filter(getattr(SkpdModel, "kode_skpd") == result['kode_skpd'])
                        skpd = skpd.one()

                        if(skpd):
                            skpd = skpd.__dict__
                            skpd.pop('_sa_instance_state', None)
                        else:
                            skpd = {}

                        result['skpd'] = skpd
                    else:
                        result['skpd'] = {}
                else:
                    result['skpd'] = {}


                # get relation to skpdsub
                if(result['kode_skpdsub'] != None):
                    if(len(result['kode_skpdsub'].split()) > 0):
                        skpdsub = db.session.query(SkpdSubModel)
                        skpdsub = skpdsub.options(
                            defer("cuid"),
                            defer("cdate"))
                        skpdsub = skpdsub.filter(getattr(SkpdSubModel, "kode_skpd") == result['skpd']['kode_skpd'])
                        skpdsub = skpdsub.filter(getattr(SkpdSubModel, "kode_skpdsub") == result['kode_skpdsub'])
                        skpdsub = skpdsub.limit(1)
                        skpdsub = skpdsub.one()

                        if(skpdsub):
                            skpdsub = skpdsub.__dict__
                            skpdsub.pop('_sa_instance_state', None)
                        else:
                            skpdsub = {}

                        result['skpdsub'] = skpdsub
                    else:
                        result['skpdsub'] = {}
                else:
                    result['skpdsub'] = {}

                # get relation to skpdunit
                if(result['kode_skpdunit'] != None):
                    if(len(result['kode_skpdunit'].split()) > 0):
                        skpdunit = db.session.query(SkpdUnitModel)
                        skpdunit = skpdunit.options(
                            defer("cuid"),
                            defer("cdate"))
                        skpdunit = skpdunit.filter(getattr(SkpdSubModel, "kode_skpd") == result['skpd']['kode_skpd'])
                        skpdunit = skpdunit.filter(getattr(SkpdSubModel, "kode_skpdsub") == result['skpdsub']['kode_skpdsub'])
                        skpdunit = skpdunit.filter(getattr(SkpdUnitModel, "kode_skpdunit") == result['kode_skpdunit'])
                        skpdunit = skpdunit.limit(1)
                        skpdunit = skpdunit.one()

                        if(skpdunit):
                            skpdunit = skpdunit.__dict__
                            skpdunit.pop('_sa_instance_state', None)
                        else:
                            skpdunit = {}

                        result['skpdunit'] = skpdunit
                    else:
                        result['skpdunit'] = {}
                else:
                    result['skpdunit'] = {}

                # get relation to role
                role = db.session.query(UserAppRoleModel)
                role = role.options(
                    defer("cuid"),
                    defer("cdate"),
                    defer("user_id"),
                    defer("notes"))
                role = role.filter(getattr(UserAppRoleModel, "user_id") == result['id'])
                role = role.all()

                roles = []
                apps = []
                for role in role:
                    if(role):
                        role = role.__dict__
                        role.pop('_sa_instance_state', None)
                        role.pop('id', None)

                        # get relation to rl
                        rl = db.session.query(RoleModel)
                        rl = rl.options(
                            defer("cuid"),
                            defer("cdate"))
                        rl = rl.get(role['role_id'])
                        role.pop('role_id', None)

                        if(rl):
                            rl = rl.__dict__
                            rl.pop('_sa_instance_state', None)
                            role['id'] = rl['id']
                            role['name'] = rl['name']
                            role['title'] = rl['title']
                        else:
                            role['id'] = ''
                            role['name'] = ''
                            role['title'] = ''

                        # get relation to app
                        app = db.session.query(AppModel)
                        app = app.options(
                            defer("cuid"),
                            defer("cdate"),
                            defer("is_deleted"),
                            defer("is_active"))
                        app = app.get(role['app_id'])
                        role.pop('app_id', None)

                        if(app):
                            app = app.__dict__
                            app.pop('_sa_instance_state', None)
                            app['name'] = app['name']
                            app['code'] = app['code']
                            app['url'] = app['url']
                        else:
                            app['name'] = ''
                            app['code'] = ''
                            app['url'] = ''

                    else:
                        role = {}

                    roles.append(role)
                    apps.append(app)

                result['role'] = roles
                result['app'] = apps

                if result['agreement_id']:
                    result_agreement = db.session.query(AgreementModel)
                    result_agreement = result_agreement.get(result['agreement_id'])
                    if result_agreement:
                        result_agreement = result_agreement.__dict__
                        result_agreement.pop('_sa_instance_state', None)
                        result_agreement['cdate'] = result_agreement['cdate'].strftime("%Y-%m-%d %H:%M:%S")
                        result_agreement['mdate'] = result_agreement['mdate'].strftime("%Y-%m-%d %H:%M:%S")
                        if result_agreement['is_active'] == True:
                            result['is_agree'] = True
                        else:
                            result['is_agree'] = False
                        result['agreement'] = result_agreement
                    else:
                        result['is_agree'] = False
                else:
                    result['is_agree'] = False

            else:
                result = {}

            # change into dict
            user = result

            # check if empty
            if user:
                return True, user
            else:
                return False, {}

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def get_by_username(self, username):

        try:
            # where
            where = {'username': username}

            # query postgresql
            result = db.session.query(UserModel)
            for attr, value in where.items():
                result = result.filter(getattr(UserModel, attr) == value)
            result = result.all()

            re = []
            for res in result:
                res = res.__dict__
                res.pop('_sa_instance_state', None)


            # change into dict
            user = result

            # check if empty
            if user:
                return True, user
            else:
                return False, {}

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def create(self, json: dict):

        try:
            # generate json data
            json_send = {}
            json_send = json
            json_send["password"] = helper.generate_hash(json['password'])
            json_send["cdate"] = helper.localDate()

            # find if exist
            exist, data = self.get_by_username(json_send["username"])

            # execute database
            if(exist == False):
                # prepare data model
                result = UserModel(**json_send)

                # execute database
                db.session.add(result)
                db.session.commit()
                result = result.to_dict()
                res, user = self.get_by_id(result['id'])

                # check if exist
                if(res):
                    return True, user, ""
                else:
                    return False, {}, ""

            else:
                return False, {}, "username already exist"

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def update(self, where: dict, json: dict):

        try:
            # generate json data
            json_send = {}
            json_send = json

            if "password" in json_send:
                json_send["password"] = helper.generate_hash(json['password'])
                # json_send["mdate"] = helper.localDate()

            try:
                # prepare data model
                result = db.session.query(UserModel)
                for attr, value in where.items():
                    result = result.filter(getattr(UserModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, user = self.get_by_id(where["id"])

                # check if empty
                if (res):
                    return True, user
                else:
                    return False, {}

            except Exception as e:
                # fail response
                raise ErrorMessage("Id not found " + str(e), 500, 1, {})

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def delete(self, where: dict):

        try:
            # query
            where = where

            try:
                # prepare data model
                result = db.session.query(UserModel)
                for attr, value in where.items():
                    result = result.filter(getattr(UserModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, user = self.get_by_id(where["id"])

                # check if exist
                if(res):
                    return False
                else:
                    return True

            except Exception as e:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def get_password(self,id):
        try:
            # execute database
            result = db.session.query(UserModel.password)
            result = result.filter(UserModel.id == id)
            result = result.one()

            output = {}
            output['pwd'] = result[0]

            # check if empty
            if output:
                return True, output
            else:
                return False, {}

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def change(self,id,json):
        try:
            ret, res = self.get_password(id)
            param = {}
            user_pass_ok = helper.verify_hash(json["old"], res["pwd"])

            if(user_pass_ok):
                param["password"] = json['password']
                status, result = self.update({'id':id},param)
                return True, result, "Success update password"
            else:
                return False, {}, "Old password not match"

        except Exception as e:
            return str(e)
