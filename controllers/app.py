from settings.configuration import Configuration
from helpers import Helper
from datetime import datetime
from dateutil import parser
from exceptions import BadRequest
from exceptions import ErrorMessage
from exceptions import SuccessMessage
from models import AppModel
from models import SkpdModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy.orm import defer
from sqlalchemy import or_
from sqlalchemy import join
from sqlalchemy import func
from sqlalchemy import cast
import sqlalchemy
import json , pytz


configuration = Configuration()
helper = Helper()

# referensi query model sql alchemy
# https://docs.sqlalchemy.org/en/latest/orm/query.html


class AppController(object):

    def __init__(self, **kwargs):
        pass

    def get_all(self, where: dict, search, sort, limit, skip):

        try:
            # query postgresql
            result = db.session.query(AppModel)
            result = result.options(
                defer("cuid"),
                defer("cdate"),
                defer("muid"),
                defer("mdate"),
                defer("is_deleted"))
            for attr, value in where.items():
                result = result.filter(getattr(AppModel, attr) == value)
            result = result.filter(or_(cast(getattr(AppModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in AppModel.__table__.columns ))
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            app = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)

                if(temp['kode_skpd'] != None):
                    # get relation to skpd
                    skpd = db.session.query(SkpdModel)
                    skpd = skpd.filter(getattr(SkpdModel, 'kode_skpd') == temp['kode_skpd'])
                    skpd = skpd.one()

                    if(skpd):
                        skpd = skpd.__dict__
                        skpd.pop('_sa_instance_state', None)
                    else:
                        skpd = {}

                    temp['skpd'] = skpd
                else:
                    temp['skpd'] = {}

                app.append(temp)

            # check if empty
            app = list(app)
            if (len(app) > 0):
                return True, app
            else:
                return False, []

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def get_count(self, where: dict, search):

        try:
            # query postgresql
            result = db.session.query(AppModel.id)
            for attr, value in where.items():
                result = result.filter(getattr(AppModel, attr) == value)
            result = result.filter(or_(cast(getattr(AppModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in AppModel.__table__.columns ))
            result = result.count()

            # change into dict
            app = {}
            app['count'] = result

            # check if empty
            if app:
                return True, app
            else:
                return False, {}

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def get_by_id(self, id):

        try:
            # execute database
            result = db.session.query(AppModel)
            result = result.options(
                defer("cuid"),
                defer("cdate"),
                defer("muid"),
                defer("mdate"),
                defer("is_deleted"))
            result = result.get(id)

            if(result):
                result = result.__dict__
                result.pop('_sa_instance_state', None)

                if(result['kode_skpd'] != None):
                    # get relation to skpd
                    skpd = db.session.query(SkpdModel)
                    skpd = skpd.filter(getattr(SkpdModel, 'kode_skpd') == result['kode_skpd'])
                    skpd = skpd.one()

                    if(skpd):
                        skpd = skpd.__dict__
                        skpd.pop('_sa_instance_state', None)
                    else:
                        skpd = {}

                    result['skpd'] = skpd
                else:
                    result['skpd'] = {}

            else:
                result = {}

            # change into dict
            app = result

            # check if empty
            if app:
                return True, app
            else:
                return False, {}

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def create(self, json: dict):

        try:
            # generate json data
            json_send = {}
            json_send = json
            # json_send["cdate"] = helper.localDate()

            # prepare data model
            result = AppModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, app = self.get_by_id(result['id'])

            # check if exist
            if(res):
                return True, app
            else:
                return False, {}

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def update(self, where: dict, json: dict):

        try:
            # generate json data
            json_send = {}
            json_send = json
            # json_send["mdate"] = helper.localDate()

            try:
                # prepare data model
                result = db.session.query(AppModel)
                for attr, value in where.items():
                    result = result.filter(getattr(AppModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, app = self.get_by_id(where["id"])

                # check if empty
                if (res):
                    return True, app
                else:
                    return False, {}

            except Exception as e:
                # fail response
                raise ErrorMessage("Id not found, " + str(e), 500, 1, {})

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def delete(self, where: dict):

        try:
            # query
            where = where

            try:
                # prepare data model
                result = db.session.query(AppModel)
                for attr, value in where.items():
                    result = result.filter(getattr(AppModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, app = self.get_by_id(where["id"])

                # check if exist
                if(res):
                    return False
                else:
                    return True

            except Exception as e:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})
