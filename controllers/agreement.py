from settings.configuration import Configuration
from helpers import Helper
from datetime import datetime
from dateutil import parser
from exceptions import BadRequest
from exceptions import ErrorMessage
from exceptions import SuccessMessage
from models import AgreementModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy.orm import defer
from sqlalchemy import or_
from sqlalchemy import join
from sqlalchemy import func
from sqlalchemy import cast
import sqlalchemy
import json


configuration = Configuration()
helper = Helper()

# referensi query model sql alchemy
# https://docs.sqlalchemy.org/en/latest/orm/query.html


class AgreementController(object):

    def __init__(self, **kwargs):
        pass

    def get_all(self, where: dict, search, sort, limit, skip):

        try:
            # query postgresql
            result = db.session.query(AgreementModel)
            for attr, value in where.items():
                result = result.filter(getattr(AgreementModel, attr) == value)
            result = result.filter(or_(cast(getattr(AgreementModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in AgreementModel.__table__.columns ))
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            agreement = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)
                temp.update({"cdate":temp['cdate'].strftime("%Y-%m-%d %H:%M:%S")})
                temp.update({"mdate":temp['mdate'].strftime("%Y-%m-%d %H:%M:%S")})
                agreement.append(temp)

            # check if empty
            agreement = list(agreement)
            if (len(agreement) > 0):
                return True, agreement
            else:
                return False, []

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def get_count(self, where: dict, search):

        try:
            # query postgresql
            result = db.session.query(AgreementModel.id)
            for attr, value in where.items():
                result = result.filter(getattr(AgreementModel, attr) == value)
            result = result.filter(or_(cast(getattr(AgreementModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in AgreementModel.__table__.columns ))
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def get_by_id(self, id):

        try:
            # execute database
            result = db.session.query(AgreementModel)
            result = result.get(id)

            if(result):
                result = result.__dict__
                result.pop('_sa_instance_state', None)
                result.update({"cdate":result['cdate'].strftime("%Y-%m-%d %H:%M:%S")})
                result.update({"mdate":result['mdate'].strftime("%Y-%m-%d %H:%M:%S")})
            else:
                result = {}

            # change into dict
            agreement = result

            # check if empty
            if agreement:
                return True, agreement
            else:
                return False, {}

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def create(self, json: dict):

        try:
            # get jwt payload
            jwt = helper.read_jwt()

            # generate json data
            json_send = {}
            json_send = json
            # json_send["cdate"] = helper.localDate()
            # json_send["cuid"] = jwt['id']

            # prepare data model
            result = AgreementModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, agreement = self.get_by_id(result['id'])

            # check if exist
            if(res):
                return True, agreement
            else:
                return False, {}

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def update(self, where: dict, json: dict):

        try:
            # get jwt payload
            jwt = helper.read_jwt()

            # generate json data
            json_send = {}
            json_send = json
            # json_send["mdate"] = helper.localDate()
            # json_send["muid"] = jwt['id']

            try:
                # prepare data model
                result = db.session.query(AgreementModel)
                for attr, value in where.items():
                    result = result.filter(getattr(AgreementModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, agreement = self.get_by_id(where["id"])

                # check if empty
                if (res):
                    return True, agreement
                else:
                    return False, {}

            except Exception as e:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def delete(self, where: dict):

        try:
            # query
            where = where

            try:
                # prepare data model
                result = db.session.query(AgreementModel)
                for attr, value in where.items():
                    result = result.filter(getattr(AgreementModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, agreement = self.get_by_id(where["id"])

                # check if exist
                if(res):
                    return False
                else:
                    return True

            except Exception as e:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})
