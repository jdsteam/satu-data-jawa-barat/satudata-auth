from settings.configuration import Configuration
from helpers import Helper
from datetime import datetime
from dateutil import parser
from exceptions import BadRequest
from exceptions import ErrorMessage
from exceptions import SuccessMessage
from models import UserAppRoleModel
from models import AppModel
from models import RoleModel
from models import UserModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy.orm import defer
from sqlalchemy import or_
from sqlalchemy import join
from sqlalchemy import func
from sqlalchemy import cast
import sqlalchemy
import json, pytz


configuration = Configuration()
helper = Helper()

# referensi query model sql alchemy
# https://docs.sqlalchemy.org/en/latest/orm/query.html


class UserAppRoleController(object):

    def __init__(self, **kwargs):
        pass

    def get_all(self, where: dict, search, sort, limit, skip):

        try:
            # query postgresql
            result = db.session.query(UserAppRoleModel)
            result = result.options(
                defer("cuid"),
                defer("cdate"))
            for attr, value in where.items():
                result = result.filter(getattr(UserAppRoleModel, attr) == value)
            result = result.filter(or_(cast(getattr(UserAppRoleModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in UserAppRoleModel.__table__.columns ))
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            user_app_role = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)


                # get relation to user
                user = db.session.query(UserModel)
                user = user.options(
                    defer("password"),
                    defer("cuid"),
                    defer("cdate"),
                    defer("is_deleted"),
                    defer("is_active"))
                user = user.get(temp['user_id'])

                if(user):
                    user = user.__dict__
                    user.pop('_sa_instance_state', None)
                else:
                    user = {}

                temp['user'] = user

                # get relation to app
                app = db.session.query(AppModel)
                app = app.options(
                    defer("cuid"),
                    defer("cdate"),
                    defer("is_deleted"),
                    defer("is_active"))
                app = app.get(temp['app_id'])

                if(app):
                    app = app.__dict__
                    app.pop('_sa_instance_state', None)
                else:
                    app = {}

                temp['app'] = app

                # get relation to role
                role = db.session.query(RoleModel)
                role = role.options(
                    defer("cuid"),
                    defer("cdate"),
                    defer("is_deleted"),
                    defer("is_active"))
                role = role.get(temp['role_id'])

                if(role):
                    role = role.__dict__
                    role.pop('_sa_instance_state', None)
                else:
                    role = {}

                temp['role'] = role


                user_app_role.append(temp)

            # check if empty
            user_app_role = list(user_app_role)
            if (len(user_app_role) > 0):
                return True, user_app_role
            else:
                return False, []

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def get_count(self, where: dict, search):

        try:
            # query postgresql
            result = db.session.query(UserAppRoleModel.id)
            for attr, value in where.items():
                result = result.filter(getattr(UserAppRoleModel, attr) == value)
            result = result.filter(or_(cast(getattr(UserAppRoleModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in UserAppRoleModel.__table__.columns ))
            result = result.count()

            # change into dict
            user_app_role = {}
            user_app_role['count'] = result

            # check if empty
            if user_app_role:
                return True, user_app_role
            else:
                return False, {}

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def get_by_id(self, id):

        try:
            # execute database
            result = db.session.query(UserAppRoleModel)
            result = result.options(
                defer("cuid"),
                defer("cdate"))
            result = result.get(id)
            if(result):
                result = result.__dict__
                result.pop('_sa_instance_state', None)

                # get relation to user
                user = db.session.query(UserModel)
                user = user.options(
                    defer("password"),
                    defer("cuid"),
                    defer("cdate"),
                    defer("is_deleted"),
                    defer("is_active"))
                user = user.get(result['user_id'])

                if(user):
                    user = user.__dict__
                    user.pop('_sa_instance_state', None)
                else:
                    user = {}

                result['user'] = user

                # get relation to app
                app = db.session.query(AppModel)
                app = app.options(
                    defer("cuid"),
                    defer("cdate"),
                    defer("is_deleted"),
                    defer("is_active"))
                app = app.get(result['app_id'])

                if(app):
                    app = app.__dict__
                    app.pop('_sa_instance_state', None)
                else:
                    app = {}

                result['app'] = app

                # get relation to role
                role = db.session.query(RoleModel)
                role = role.options(
                    defer("cuid"),
                    defer("cdate"),
                    defer("is_deleted"),
                    defer("is_active"))
                role = role.get(result['role_id'])

                if(role):
                    role = role.__dict__
                    role.pop('_sa_instance_state', None)
                else:
                    role = {}

                result['role'] = role


            else:
                result = {}

            # change into dict
            user_app_role = result

            # check if empty
            if user_app_role:
                return True, user_app_role
            else:
                return False, {}

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def create(self, json: dict):

        try:
            # generate json data
            json_send = {}
            json_send = json
            # json_send["cdate"] = helper.localDate()

            # prepare data model
            result = UserAppRoleModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, user_app_role = self.get_by_id(result['id'])

            # check if exist
            if(res):
                return True, user_app_role
            else:
                return False, {}

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def update(self, where: dict, json: dict):

        try:
            # generate json data
            json_send = {}
            json_send = json
            # json_send["mdate"] = helper.localDate()

            try:
                # prepare data model
                result = db.session.query(UserAppRoleModel)
                for attr, value in where.items():
                    result = result.filter(getattr(UserAppRoleModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, user_app_role = self.get_by_id(where["id"])

                # check if empty
                if (res):
                    return True, user_app_role
                else:
                    return False, {}

            except Exception as e:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def delete(self, where: dict):

        try:
            # query
            where = where

            try:
                # prepare data model
                result = db.session.query(UserAppRoleModel)
                for attr, value in where.items():
                    result = result.filter(getattr(UserAppRoleModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, user_app_role = self.get_by_id(where["id"])

                # check if exist
                if(res):
                    return False
                else:
                    return True

            except Exception as e:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})
