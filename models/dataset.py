from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy.orm import sessionmaker, relationship
import datetime


class DatasetModel(BaseModel, db.Model):
    """Model for the dataset table"""
    __tablename__ = 'dataset'

    id = db.Column(db.Integer, primary_key=True) 
    app_id = db.Column(db.Integer, db.ForeignKey("app.id"))
    app_service_id = db.Column(db.Integer, db.ForeignKey("app_service.id"))
    kode_skpd = db.Column(db.String(255))
    kode_skpdsub = db.Column(db.String(255))
    kode_skpdunit = db.Column(db.String(255))
    name = db.Column(db.String(255))
    title = db.Column(db.String(255))
    year = db.Column(db.Integer)
    image = db.Column(db.String(255))
    description = db.Column(db.String(255))
    owner = db.Column(db.String(255))
    owner_email = db.Column(db.String(255))
    maintainer = db.Column(db.String(255))
    maintainer_email = db.Column(db.String(255))
    notes = db.Column(db.String(255))
    count_column = db.Column(db.Integer)
    count_row = db.Column(db.Integer)
    count_view = db.Column(db.Integer)
    count_access = db.Column(db.Integer)
    is_active = db.Column(db.Boolean, default=False)
    is_deleted = db.Column(db.Boolean, default=False)
    is_validate = db.Column(db.Boolean, default=False)
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)
    

def __init__(self, dataset_type_id, dataset_class_id, app_id, app_service_id, 
            sektoral_id, regional_id, license_id, kode_skpd, name, title):
    self.dataset_type_id = dataset_type_id
    self.dataset_class_id = dataset_class_id 
    self.app_id = app_id
    self.app_service_id = app_service_id
    self.sektoral_id = sektoral_id
    self.regional_id = regional_id
    self.license_id = license_id
    self.kode_skpd = kode_skpd
    self.name = name
    self.title = title
