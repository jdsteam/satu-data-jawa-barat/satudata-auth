import json, pytz
import requests
import jwt

from flask import Blueprint
from flask import request
from flask import Response
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from exceptions import BadRequest
from exceptions import ErrorMessage
from exceptions import SuccessMessage
from settings import configuration
from controllers import AuthController
from controllers import UserController
from controllers import RoleController
from helpers import Helper
import datetime
from models import HistoryLoginModel
from helpers.postgre_alchemy import postgre_alchemy as db


bp = Blueprint(__name__, 'user')
authController = AuthController()
userController = UserController()
roleController = RoleController()
helper = Helper()


@bp.route("/auth/login", methods=["POST"])
# @jwt_check()
def login():

    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        # insert user
        res, user, message = authController.login(json_request)

        # insert history login
        if(res):
            json_history = {}
            json_history['ip'] = request.remote_addr
            json_history['browser'] = request.headers.get('User-Agent')
            json_history['type'] = 'login'
            json_history["user_id"] = user['id']
            json_history["datetime"] = helper.localDate()
            try:
                url = 'http://www.geoplugin.net/json.gp?ip='+request.remote_addr
                r = requests.get(url)
                j = json.loads(r.text)
                json_history['country_code'] = j['geoplugin_countryCode']
                json_history['country'] = j['geoplugin_countryName']
                json_history['region'] = j['geoplugin_region']
                json_history['lat'] = j['geoplugin_latitude']
                json_history['long'] = j['geoplugin_longitude']
            except Exception as e:
                pass
            # prepare data model
            result = HistoryLoginModel(**json_history)
            # execute database
            db.session.add(result)
            db.session.commit()


        # response
        if(res):
            # success response
            response = {"message": "Login successfull",
                        "error": 0, "data": user}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Login failed, " + message,
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as e:
        # fail response
        response = {"message": "Internal Server Error. " + str(e),
                    "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500

@bp.route("/auth/logout", methods=["GET"])
# @jwt_check()
def logout():

    try:
        # jwt
        jwt = helper.read_jwt()

        # insert history login
        json_history = {}
        json_history['ip'] = request.remote_addr
        json_history['browser'] = request.headers.get('User-Agent')
        json_history['type'] = 'logout'
        json_history["user_id"] = jwt['id']
        json_history["datetime"] = helper.localDate()
        try:
            url = 'http://www.geoplugin.net/json.gp?ip='+request.remote_addr
            r = requests.get(url)
            j = json.loads(r.text)
            json_history['country_code'] = j['geoplugin_countryCode']
            json_history['country'] = j['geoplugin_countryName']
            json_history['region'] = j['geoplugin_region']
            json_history['lat'] = j['geoplugin_latitude']
            json_history['long'] = j['geoplugin_longitude']
        except Exception as e:
            pass
        # prepare data model
        result = HistoryLoginModel(**json_history)
        # execute database
        db.session.add(result)
        db.session.commit()
        result = result.to_dict()

        # response
        if(result):
            # success response
            response = {"message": "Logout successfull",
                        "error": 0, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Logout failed",
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as e:
        # fail response
        print(str(e))
        response = {"message": "Internal Server Error. JWT not found.",
                    "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500

@bp.route("/auth/check", methods=["GET"])
def check():

    app_code = request.args.get("app_code")
    controller = request.args.get("controller")
    controller = controller.split("/")
    controller = controller[0]
    action = request.args.get("action")
    token = request.headers.get('Authorization', None)

    return check_is_backend(app_code, controller, action, token)

def check_is_backend(app_code, controller, action, token):
    try:
        # get permission is backend
        res, permission = authController.check_is_backend(app_code, controller, action)

        if(res):
            # is backend
            if(permission['is_backend'] == True):
                return check_user(app_code, controller, action, token)

            # is bigdata
            else:
                # classification publik
                if((permission['dataset_class']['name'].lower() == 'publik') or (permission['dataset_class']['id'] == 3)):
                    response = {"message": "Access Granted, Data is public",
                                "error": 0, "data": {}}
                    return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

                # classification internal
                elif((permission['dataset_class']['name'].lower() == 'internal') or (permission['dataset_class']['id'] == 5)):
                    if token is not None:
                        respon, payload = check_token(app_code, controller, action, token)
                        if(respon):
                            # user is dinas
                            if(payload['is_external'] == False):
                                response = {"message": "Access Granted, Data is internal",
                                            "error": 0, "data": {}}
                                return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
                            # user is external
                            else:
                                return check_user(app_code, controller, action, token)
                        else:
                            return Response(json.dumps(payload, cls=JSONEncoder), mimetype='application/json'), 403
                    else:
                        # jika ga ada token
                        response = {"message": "Token Missing","error": 1, "data": {}}
                        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403

                # classification dikecualikan
                elif((permission['dataset_class']['name'].lower() == 'dikecualikan') or (permission['dataset_class']['id'] == 4)):
                    return check_user(app_code, controller, action, token)

                # not found
                else:
                    return check_user(app_code, controller, action, token)
        else:
            # code tidak ditemukan
            response = {"message": "App Code, Controller, or Action not found",
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403

    except Exception as e:
        # fail query
        response = {"message": "Failed get detail service, " + str(e),
                    "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403

def check_token(app_code, controller, action, token):

    if token:
        try:
            # decode jwt
            token = token.split(" ")
            payload = jwt.decode(token[1], configuration.jwt_secret_key, algorithm=[configuration.jwt_algorithm])
            return True, payload

        except jwt.DecodeError:
            # jika token salah (secret key / algoitmanya)
            response = {"message": "Token Invalid",
                        "error": 1, "data": {}}
            return False, response

        except jwt.ExpiredSignatureError:
            # jika token sudah expired
            response = {"message": "Token Expired",
                        "error": 1, "data": {}}
            return False, response

        except Exception as e:
            # jika lainnya
            response = {"message":  str(e),
                        "error": 1, "data": {}}
            return False, response

    else:
        # jika ga ada token
        response = {"message": "Token Missing",
                    "error": 1, "data": {}}
        return False, response

def check_user(app_code, controller, action, token):
    try:
        if token:

            # decode jwt
            token = token.split(" ")
            payload = jwt.decode(token[1], configuration.jwt_secret_key, algorithm=[configuration.jwt_algorithm])

            # pencarian user
            res, user = userController.get_by_id(payload["id"])

            # cek user
            try:
                if(("id" in payload) and ("username" in payload) and ("role" in payload) and ("id" in payload["role"])):
                    # cek username dan id
                    if(user["username"] == payload["username"]):
                        return check_permission(app_code, controller, action, token, payload)

                    else:
                        # username tidak cocok dengan id
                        response = {"message": "User not match",
                                    "error": 1, "data": {}}
                        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403
                else:
                    # id tidak ditemukan
                    response = {"message": "User not found",
                                "error": 1, "data": {}}
                    return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403

            except Exception as e:
                # id tidak ditemukan
                response = {"message": "User not valid",
                            "error": 1, "data": {}}
                return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403

        else:
            response = {"message": "Token Missing",
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403

    except Exception as e:
        # id tidak ditemukan
        response = {"message": "Failed get user, " + str(e),
                    "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403

def check_permission(app_code, controller, action, token, payload):

    try:
        res, roles = authController.permission_by_userid(payload["id"], app_code, controller, action)

        # jika ada
        if(res):
            # jika boleh
            if(roles[0]["enable"] == True):
                # url yg diakses sesuai dengan rolenya
                response = {"message": "Access Granted",
                            "error": 0, "data": {}}
                return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

            else:
                # url yg diakses tidak sesuai dengan rolenya
                response = {"message": "Access Forbidden",
                            "error": 1, "data": {}}
                return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403

        else:
            # url yg diakses tidak sesuai dengan rolenya
            response = {"message": "URL App, Controller, or Action not found",
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403

    except Exception as e:
        # id tidak ditemukan
        response = {"message": "Failed get permission",
                    "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403

@bp.route("/auth/reset", methods=["POST"])
def resetPassword():

    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        # insert user
        status, res = authController.resetPassword(json_request)
        # response
        if(status):
            # success response
            response = {"message": "Reset Password Berhasil.",
                        "error": 0, "data": res}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Reset Password Gagal, " + res,
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as e:
        # fail response
        response = {"message": "Internal Server Error. " + str(e),
                    "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500
