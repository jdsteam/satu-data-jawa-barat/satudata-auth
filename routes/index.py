from flask import Blueprint
from flask import jsonify
from settings import configuration
import json

bp = Blueprint(__name__, 'index')


# DONE
# @bp.route("/", methods=["GET"])
# def index():
#     response = {"error": 0,
#                 "message": "Welcome to Satu Data Auth", "data": {"doc":  "/documentation"}}
#     return jsonify(response)


@bp.route("/is_alive", methods=["GET"])
def is_alive():
    # success response format
    response = {"error": 0, "message": "Connected", "data": []}

    return jsonify(response)
