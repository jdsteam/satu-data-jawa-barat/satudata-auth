import json
from flask import Blueprint
from flask import request
from flask import Response
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from exceptions import BadRequest
from exceptions import ErrorMessage
from exceptions import SuccessMessage

from controllers import UserPermissionController

bp = Blueprint(__name__, 'user_permission')
userPermissionController = UserPermissionController()


@bp.route("/user_permission", methods=["GET"])
@jwt_check()
def get_all():

    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'id:ASC')
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", "\"")
        where = json.loads(where)

        if(count):        
            # call function get
            res, user_permission = userPermissionController.get_count(where, search)
        else:
            # call function get
            res, user_permission = userPermissionController.get_all(where, search, sort, limit, skip)

        # response
        if(res):
            # success response
            response = {"message": "Get data successfull",
                        "error": 0, "data": user_permission}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Data not found",
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as e:
        # fail response
        response = {"message": "Internal Server Error. " + str(e),
                    "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


@bp.route("/user_permission/<id>", methods=["GET"])
@jwt_check()
def get_by_id(id):

    try:
        # call function get by id
        res, user_permission = userPermissionController.get_by_id(id)

        # response
        if(res):
            # success response
            response = {"message": "Get detail data successfull",
                        "error": 0, "data": user_permission}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Data not found",
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as e:
        # fail response
        response = {"message": "Internal Server Error. " + str(e),
                    "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


@bp.route("/user_permission", methods=["POST"])
@jwt_check()
def create():

    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        if(isinstance(json_request, list)): 
            # insert dataset_tag
            res, user_permission = userPermissionController.createMultiple(json_request)
        else:  
            # insert dataset_tag
            res, user_permission = userPermissionController.create(json_request)
  
        # response
        if(res):
            # success response
            response = {"message": "Create data successfull",
                        "error": 0, "data": user_permission}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Create data failed, ",
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as e:
        # fail response
        response = {"message": "Internal Server Error. " + str(e),
                    "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


@bp.route("/user_permission/<id>", methods=["PUT"])
@jwt_check()
def update(id):

    try:
        # filter data
        where = {'id': id}

        # prepare json data
        json_request = request.get_json(silent=True)

        # update user_permission
        res, user_permission = userPermissionController.update(where, json_request)

        # response
        if(res):
            # success response
            response = {"message": "Update data successfull",
                        "error": 0, "data": user_permission}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Update data failed",
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as e:
        # fail response
        response = {"message": "Internal Server Error. " + str(e),
                    "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


@bp.route("/user_permission/<id>", methods=["DELETE"])
@jwt_check()
def delete(id):

    try:
        # filter data
        where = {'id': id}

        # delete user_permission
        res = userPermissionController.delete(where)

        # response
        if(res):
            # success response
            response = {"message": "Delete data successfull",
                        "error": 0, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Delete data failed",
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as e:
        # fail response
        response = {"message": "Internal Server Error. " + str(e),
                    "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


@bp.route("/user_permission/access", methods=["GET"])
@jwt_check()
def check_private():
    try:
        # filter data
        dataset_id = request.args.get("dataset_id", "") 
  
        if(dataset_id):        
            # call function get
            res, user_permission = userPermissionController.get_skpd(dataset_id)
        else:
            # success response but no data
            response = {"message": "Parameter dataset_id not found",
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

        # response
        if(res):
            # success response
            response = {"message": "Get data successfull",
                        "error": 0, "data": user_permission}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Data not found",
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as e:
        # fail response
        response = {"message": "Internal Server Error. " + str(e),
                    "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


@bp.route("/user_permission/access", methods=["POST"])
@jwt_check()
def create_private():

    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        if(isinstance(json_request, list)): 
            # insert dataset_tag 
            res, user_permission = userPermissionController.createPrivateMultiple(json_request)
        else:  
            # insert dataset_tag
            res, user_permission = userPermissionController.createPrivate(json_request)
  
        # response
        if(res):
            # success response
            response = {"message": "Create data successfull",
                        "error": 0, "data": user_permission}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Create data failed, ",
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as e:
        # fail response
        response = {"message": "Internal Server Error. " + str(e),
                    "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


@bp.route("/user_permission/access", methods=["PUT"])
@jwt_check()
def update_private():

    try:
        # prepare json data 
        dataset_id = request.args.get("dataset_id", "") 
        json_request = request.get_json(silent=True)

        
        if(dataset_id):         
            if(isinstance(json_request['old'], list) and isinstance(json_request['new'], list)): 
                # insert dataset_tag 
                res, user_permission = userPermissionController.updatePrivateMultiple(json_request, dataset_id)
            else:  
                # insert dataset_tag
                res, user_permission = userPermissionController.updatePrivate(json_request, dataset_id)
        else:
            # success response but no data
            response = {"message": "Parameter dataset_id not found",
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
 
        # response
        if(res):
            # success response
            response = {"message": "Create data successfull",
                        "error": 0, "data": user_permission}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Create data failed, ",
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as e:
        # fail response
        response = {"message": "Internal Server Error. " + str(e),
                    "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


@bp.route("/user_permission/cek", methods=["GET"])
@jwt_check()
def cekprivate():
 
    userPermissionController.insertWalidata('0', '0')
        